## RQ3A.210605.005
- Manufacturer: xiaomi
- Platform: 
- Codename: juice
- Brand: Xiaomi
- Flavor: lineage_juice-userdebug
- Release Version: 11
- Id: RQ3A.210605.005
- Incremental: eng.gtrcra.20210628.090409
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/lineage_juice/juice:11/RQ3A.210605.005/gtrcraft06280903:userdebug/release-keys
- OTA version: 
- Branch: RQ3A.210605.005
- Repo: xiaomi_juice_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
